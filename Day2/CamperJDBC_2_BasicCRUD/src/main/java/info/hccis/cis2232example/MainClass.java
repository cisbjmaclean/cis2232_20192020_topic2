package info.hccis.cis2232example;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add camper\nU) Update camper DOB\nD) Delete camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/cis2232/campersRandom.json";

    public static void main(String[] args) throws IOException {
        ArrayList<Camper> theList = new ArrayList();
        CamperDAO camperDAO = new CamperDAO();
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper(true);
                    camperDAO.insert(newCamper);
                    break;
                case "U":
                    //System.out.println("Picked A");
                    System.out.println("Enter registration id");
                    int registrationIdForUpdate = FileUtility.getInput().nextInt();
                    FileUtility.getInput().nextLine(); //burn
                    Camper camper = camperDAO.select(registrationIdForUpdate);
                    if(camper == null){
                        System.out.println("Camper not found");
                    }else{
                        camper.updateDOB();
                        camperDAO.update(camper);
                        System.out.println("Camper updated");
                    }
                    break;
                case "D":
                    //System.out.println("Picked A");
                    System.out.println("Enter registration id");
                    int registrationId = FileUtility.getInput().nextInt();
                    FileUtility.getInput().nextLine(); //burn
                    camperDAO.delete(registrationId);
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    theList = camperDAO.selectAll();
                    for (Camper camper2 : theList) {
                        System.out.println(camper2);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }

        } while (!option.equalsIgnoreCase(
                "x"));
    }
}
