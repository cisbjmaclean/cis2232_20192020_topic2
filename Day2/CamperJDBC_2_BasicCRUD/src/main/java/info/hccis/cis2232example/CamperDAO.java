package info.hccis.cis2232example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classes related to working with the Campers with JDBC
 *
 * @author bjmaclean
 * @since 20180928
 */
public class CamperDAO {

    Connection conn = null;
    Statement stmt;

    /**
     * Default constructor will setup the connection and statement objects to be
     * used by this CamperDAO instance.
     *
     * @since 20180928
     * @author BJM
     */
    public CamperDAO() {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_camper",
                    "root",
                    "");

            stmt = conn.createStatement();
        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }
    }

    /**
     * This method will select all campers, create camper objects for each, add
     * them to an ArrayList, then return the array list.
     *
     * @since 20180928
     * @author BJM
     */
    public ArrayList<Camper> selectAll() {
        ArrayList<Camper> campers;
        try {
            //Next select all the rows and display them here...
            campers = new ArrayList();

            ResultSet rs = stmt.executeQuery("select * from Camper");
            while (rs.next()) {
                Camper camper = new Camper();
                camper.setRegistrationId(rs.getInt("registrationId"));
                camper.setFirstName(rs.getString(2));
                camper.setLastName(rs.getString(3));
                camper.setDob(rs.getString(4));
                campers.add(camper);
            }

            return campers;
        } catch (SQLException ex) {
            Logger.getLogger(CamperDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * This method will return a particular camper.
     *
     * @since 20180928
     * @author BJM
     */
    public Camper select(int registrationId) {

        try {
            PreparedStatement theStatement = null;
            String sqlString = "select * from Camper where registrationId=?";
            theStatement = conn.prepareStatement(sqlString);
            theStatement.setInt(1, registrationId);
            ResultSet rs = theStatement.executeQuery();            
            
            Camper camper = null;
            
            /*
            The camper is initially set to null.  Will loop through the result set
            to set the values of the new camper object.  If we don't find one then will
            be returning null.
            */

            while (rs.next()) {
                camper = new Camper();
                camper.setRegistrationId(rs.getInt("registrationId"));
                camper.setFirstName(rs.getString(2));
                camper.setLastName(rs.getString(3));
                camper.setDob(rs.getString(4));
            }

            
            return camper;
        } catch (SQLException ex) {
            Logger.getLogger(CamperDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * This method will delete the camper associated with the registration id.
     *
     * @param registrationId Identifies the camper to be deleted.
     * @since 20180928
     * @author BJM
     */
    public void delete(int registrationId) {
        try {
            PreparedStatement deleteStatement = null;
            String sqlString = "delete from Camper where registrationId=?";
            deleteStatement = conn.prepareStatement(sqlString);
            deleteStatement.setInt(1, registrationId);
            
            deleteStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * This method will insert the input camper. It will notify the console if
     * an exception is encountered (IE camper already exists).
     *
     * @param camper The camper to insert
     * @since 20180928
     * @author BJM
     */
    public void insert(Camper camper) {
        try {

            String query = "INSERT INTO `camper`(`registrationId`, `firstName`, `lastName`, `dob`) VALUES (";
            query += camper.getRegistrationId() + ",'" + camper.getFirstName() + "','" + camper.getLastName() + "','" + camper.getDob() + "'";
            query += ");";
            
            PreparedStatement insertStatement = null;
            String sqlString = "INSERT INTO `camper`(`registrationId`, `firstName`, `lastName`, `dob`) "
                    + "VALUES (?,?,?,?)";
            insertStatement = conn.prepareStatement(sqlString);
            insertStatement.setInt(1, camper.getRegistrationId());
            insertStatement.setString(2, camper.getFirstName());
            insertStatement.setString(3, camper.getLastName());
            insertStatement.setString(4, camper.getDob());

            
            System.out.println("About to query:" + query);
            insertStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("Could not add camper");
        }

    }

    /**
     * Will update the database based on camper provided.
     *
     * @param camper Camper to update
     * @since 20180928
     * @author BJM
     */
    public void update(Camper camper) {
        try {

            String query = "UPDATE Camper SET "
                    + "firstName='"+camper.getFirstName()+"',lastName='"+
                    camper.getLastName()+"',dob='"+camper.getDob()
                    +"' WHERE registrationId="+camper.getRegistrationId();
            System.out.println("About to query:" + query);
            stmt.executeUpdate(query);

        } catch (SQLException sqle) {
            System.out.println("Could not add camper");
        }
    }
}
