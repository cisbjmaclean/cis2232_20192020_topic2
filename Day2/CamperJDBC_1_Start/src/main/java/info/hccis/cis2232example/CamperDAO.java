package info.hccis.cis2232example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Classes related to working with the Campers with JDBC
 *
 * @author bjmaclean
 * @since 20180928
 */
public class CamperDAO {

    Connection conn = null;
    Statement stmt;

    public CamperDAO() {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_camper",
                    "cis2232_admin",
                    "Test1234");

            stmt = conn.createStatement();
        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }
    }

    public ArrayList<Camper> selectAll() {
        return new ArrayList();
    }

    public Camper select(int registrationId) {
        return new Camper();
    }

    public void delete(int registrationId) {
    }

    
    public void insert(Camper camper) {

    }

    public void update(Camper camper) {

    }
}
