package info.hccis.cis2232example;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add camper\nD) Delete camper\nS) Show campers\nX) Exit";

    public static void main(String[] args) throws IOException {
        ArrayList<Camper> theList = new ArrayList();
        CamperDAO camperDAO = new CamperDAO();
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper(true);
                    camperDAO.insert(newCamper);
                    break;
                case "D":
                    //System.out.println("Picked A");
                    System.out.println("Enter registration id");
                    int registrationId = FileUtility.getInput().nextInt();
                    FileUtility.getInput().nextLine(); //burn
                    camperDAO.delete(registrationId);
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    theList = camperDAO.selectAll();
                    for (Camper camper : theList) {
                            System.out.println(camper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }

        } while (!option.equalsIgnoreCase(
                "x"));
    }
}
